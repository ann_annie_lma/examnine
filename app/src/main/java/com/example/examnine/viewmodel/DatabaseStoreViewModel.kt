package com.example.examnine.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.examnine.data.StoreDao
import com.example.examnine.data.StoreDatabase
import com.example.examnine.model.Store
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class DatabaseStoreViewModel(application: Application) : AndroidViewModel(application) {

    val readAllData : Flow<List<Store>>
    var userDao: StoreDao = StoreDatabase.getDataBase(application).userDao()


    init {
        readAllData = userDao.getAllDataSet()
    }

    fun insertData(storeItem: List<Store>){
        viewModelScope.launch(IO) {
            userDao.insertItems(storeItem)
        }
    }


}