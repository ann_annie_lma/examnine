package com.example.examnine.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.examnine.model.Store
import com.example.examnine.network.NetWorkClient
import com.example.examnine.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NetworkStoreViewModel : ViewModel() {

    private var _response: MutableLiveData<Resource<List<Store>>> = MutableLiveData()
    val response : LiveData<Resource<List<Store>>> get() = _response

    fun load()
    {
        viewModelScope.launch {
            withContext(Dispatchers.IO)
            {
                getStoreItems()
            }
        }
    }


    private suspend fun getStoreItems()
    {

        try {
            val result = NetWorkClient.api.getData()
            val resultBody = result.body()
            if(result.code() == 200)
            {
                _response.postValue(Resource.Success(resultBody))
            }
            else
            {
                _response.postValue(Resource.Failure(result.message()))
            }

        } catch (e:Exception)
        {
            _response.postValue(Resource.Failure(e.message!!))
        }
    }
}