package com.example.examnine.model

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class Store(
    val cover: String,
    val liked: Boolean,
    val price: String,
    @PrimaryKey
    val title: String
)