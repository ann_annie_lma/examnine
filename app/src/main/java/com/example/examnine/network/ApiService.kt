package com.example.examnine.network

import com.example.examnine.model.Store
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {

    @GET(NetworkConstants.REPROSITORY_URL)
    suspend fun getData(): Response<List<Store>>
}