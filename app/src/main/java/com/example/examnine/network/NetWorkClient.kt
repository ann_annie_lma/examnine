package com.example.examnine.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetWorkClient {

    val api:ApiService by lazy {
        Retrofit.Builder().
        baseUrl(NetworkConstants.BASE_URL).
        addConverterFactory(GsonConverterFactory.create()).
        build().create(ApiService::class.java)

    }
}