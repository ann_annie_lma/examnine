package com.example.examnine.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

typealias inflation<T> = (LayoutInflater, ViewGroup, Boolean) -> T


abstract class BaseFragment<VB : ViewBinding>(private val inf:inflation<VB>) : Fragment() {

    private var _binding: VB? = null
    val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inf.invoke(inflater, container!!, false)
        return _binding!!.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        start()

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    abstract fun start()



}
