package com.example.examnine.fragments


import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.TYPE_MOBILE
import android.net.ConnectivityManager.TYPE_WIFI
import android.net.NetworkCapabilities.TRANSPORT_CELLULAR
import android.net.NetworkCapabilities.TRANSPORT_WIFI
import android.os.Build
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.example.examnine.R
import com.example.examnine.adapter.StoreAdapter
import com.example.examnine.databinding.FragmentStoreBinding
import com.example.examnine.network.Resource
import com.example.examnine.viewmodel.DatabaseStoreViewModel
import com.example.examnine.viewmodel.NetworkStoreViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.flow.collect


class StoreFragment : BaseFragment<FragmentStoreBinding>(FragmentStoreBinding::inflate) {


   private val networkStoreViewModel : NetworkStoreViewModel by viewModels()
    private lateinit var databaseStoreViewModel : DatabaseStoreViewModel
    lateinit var storeAdapter: StoreAdapter



    override fun start() {
        setUpRecycler()
        observers()
        networkStoreViewModel.load()
    }


    private fun setUpRecycler()
    {
        binding.rvItemList.apply {
            storeAdapter = StoreAdapter()
            adapter = storeAdapter
            layoutManager = GridLayoutManager(context,2,GridLayoutManager.VERTICAL,false)

        }
    }


    @SuppressLint("SetTextI18n")
    private fun observers()
    {
        databaseStoreViewModel = ViewModelProvider(this).get(DatabaseStoreViewModel::class.java)

        networkStoreViewModel.response.observe(viewLifecycleOwner, {response ->
            binding.pbNews.isVisible = true
            if(hasInternetConnection())
            {
                storeAdapter.storeItems = response.data!!
                binding.pbNews.isVisible = false
                databaseStoreViewModel.insertData(response.data)
                binding.tvNoInternetConnection.text = ""

            } else
            {
                lifecycleScope.launch {
                    databaseStoreViewModel.readAllData.collect()
                    {
                        storeAdapter.storeItems = it
                        binding.pbNews.isVisible = false
                        binding.tvNoInternetConnection.text = R.string.no_connection.toString()

                    }
                }
            }

        })


    }


    private fun hasInternetConnection(): Boolean
    {
        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            val activieNetwork = connectivityManager.activeNetwork?: return false
            val capabilities = connectivityManager.getNetworkCapabilities(activieNetwork) ?: return false

            return when{
                capabilities.hasTransport(TRANSPORT_WIFI) -> true
                capabilities.hasTransport(TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else
        {
            connectivityManager.activeNetworkInfo?.run {
                return when(type) {
                    TYPE_WIFI -> true
                    TYPE_MOBILE -> true
                    else -> false
                }
            }
        }


        return false

    }

}