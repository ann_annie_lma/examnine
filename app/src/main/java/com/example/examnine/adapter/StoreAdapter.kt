package com.example.examnine.adapter

import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.examnine.R
import com.example.examnine.databinding.StoreItemBinding
import com.example.examnine.model.Store

class StoreAdapter : RecyclerView.Adapter<StoreAdapter.StoreViewHolder>(){


    inner class StoreViewHolder(val binding: StoreItemBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun bind()
        {
            binding.apply {
                val storeItem = storeItems[adapterPosition]
                tvTitle.text = storeItem.title
                tvPrice.text = storeItem.price


                Glide.with(ivStoreItem.getContext())  // glide with this view
                    .load(storeItem.cover)
                    .placeholder(R.mipmap.ic_launcher) // default placeHolder image
                    .error(R.mipmap.ic_launcher)
                    .centerCrop()// use this image if faile
                    .into(ivStoreItem)

            }

        }



    }

    private val diffCallBack = object: DiffUtil.ItemCallback<Store>()
    {
        override fun areItemsTheSame(oldItem: Store, newItem: Store): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: Store, newItem: Store): Boolean {
            return oldItem == newItem
        }

    }

    private val differ = AsyncListDiffer(this,diffCallBack)
    var storeItems : List<Store>
        get() = differ.currentList
        set(value) {differ.submitList(value)}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreViewHolder {
        return StoreViewHolder(
            StoreItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ))
    }

    override fun onBindViewHolder(holder: StoreViewHolder, position: Int) {
        holder.bind()
    }

    override fun getItemCount() = storeItems.size
}