package com.example.examnine.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.examnine.model.Store

@Database(entities = [Store::class],version = 1,exportSchema = false)
abstract class StoreDatabase : RoomDatabase()
{
    abstract fun userDao() : StoreDao

    companion object
    {
        private var INSTANCE:StoreDatabase? = null

        fun getDataBase(context:Context) : StoreDatabase
        {
            val tempInstance = INSTANCE
            if(tempInstance != null)
            {
                return tempInstance
            }

            synchronized(this)
            {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                   StoreDatabase::class.java,
                    "user_database"
                ).build()

                INSTANCE = instance
                return instance
            }
        }
    }

}