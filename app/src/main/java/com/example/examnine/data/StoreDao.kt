package com.example.examnine.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.examnine.model.Store
import kotlinx.coroutines.flow.Flow


@Dao
interface StoreDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertItems(storeItems:List<Store>)

    @Query("SELECT * FROM Store")
    fun getAllDataSet(): Flow<List<Store>>

}